# README #

This project contains results and raw data used for in the prestudy of the ICSE-NIER paper Semi-Automated Test-Case Propagation in Fork Ecosystems by Mukelabai Mukelabai, Thorsten Berger, and Paulo Borba.

### The following are the files included: We analyzed a total of 12 ecosystems, comprising 12 mainline projects and 31 forks, total 43 projects###

* classesAndMethods_allprojects.csv: 
This file contains methods and classes in each of the 43 projects considered. The data of this file was used when matching UUTs of each test case that was missing in one or more projects of each ecosystem
* ecosystemMissingTestCaseUUTMatch.csv: 
This file contains a summary of the percentage of missing test cases in each ecosystem, that had matching UUTs in the projects where they were missing from. UUTs were matched at class and method level
* ecosystemTestCasePercentageSummary.csv: 
THis file contains a summary of the percentage of shared and missing test cases in each ecosystem. This was used to answer RQ1
* ecosystemTestCasePresence.csv: 
This is the raw results file from which we geenrated the summary in ecosystemTestCasePercentageSummary.csv
* ecosystemTestCasePresenceSummary.csv: 
This file shows the absolute numbers of shared and missing test cases in each ecosystem, grouped by quartile of projects (25%, 50%, 75%, and 100% of the projects in the ecosystem)
* finalProjectsMoreThan10TestCases.txt: 
This file contains the list of the final 43 projects we analyzed that had more than 10 test cases. The total number of test cases from all projects was 6, 569.
* projectsTestCaseCountDetail.csv: 
This file contains numbers of test cases (test methods and classes) in each project, number of class-level UUTs, and number of method-level UUTs, This was generated from the intial 26 ecosystems. We used this file to filter out all projects that had less than 10 test cases
* testsAndUUT_allprojects.csv: 
This file contains details of each test case and its corresponding UUTs (methods and classes). For each method-level UUT, we additionally indicate parameters. This was the first file we generated from which we were able to generate all other files.


### Who do I talk to? ###

* muka at chalmers dot se